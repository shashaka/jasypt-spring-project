package org.blog.test;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
@Slf4j
public class JasyptApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(JasyptApplication.class, args);
    }

    @Value("${test.password}")
    private String encryptedPassword;

    @Override
    public void run(String... args) throws Exception {
        log.info("encryptedPassword : {}", encryptedPassword);
    }
}